import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyDOzx-gtd6K87xiiNEDEKaiMFyn-eDj5eE",
    authDomain: "preavia-food-app.firebaseapp.com",
    databaseURL: "https://preavia-food-app.firebaseio.com",
    projectId: "preavia-food-app",
    storageBucket: "preavia-food-app.appspot.com",
    messagingSenderId: "738129939265",
    appId: "1:738129939265:web:9a5612f90585412e1aefc0"
};

firebase.initializeApp(firebaseConfig);
export default firebase;