import React, {Component} from 'react';
import MapView, {PROVIDER_GOOGLE, Marker, Callout} from 'react-native-maps'
import {Text, View,Image, Button,Switch, StyleSheet,TouchableOpacity, ScrollView,TextInput, TouchableWithoutFeedback,Dimensions} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/Feather';
import MapViewDirections from 'react-native-maps-directions';

const width =  Dimensions.get('window').width
const height = Dimensions.get('window').height
const heightbotones = height-200
const homePlace = {
  description: 'Home',
  geometry: { location: { lat: 48.8152937, lng: 2.4597668 } },
};
const workPlace = {
  description: 'Work',
  geometry: { location: { lat: 48.8496818, lng: 2.2940881 } },
};
export default class EditarUsuarios extends Component {
  constructor(props) {
    super(props)
    this.state = {
      Titulo:"",
      imagen:"",
      focusedLocation: {
        latitude: 36.14319077106534,
        longitude: -86.76708101838142,
        latitudeDelta: 0.00522,
        longitudeDelta: Dimensions.get("window").width / Dimensions.get("window").height * 0.00522
      }
    };
  }
  onPressZoomIn() {
    this.region = {
      latitude: this.state.focusedLocation.latitude,
      longitude: this.state.focusedLocation.longitude,
      latitudeDelta: this.state.focusedLocation.latitudeDelta * 2,
      longitudeDelta: this.state.focusedLocation.longitudeDelta * 2
    }

    this.setState({
      focusedLocation: {
        latitudeDelta: this.region.latitudeDelta,
        longitudeDelta: this.region.longitudeDelta,
        latitude: this.region.latitude,
        longitude: this.region.longitude
      }
    })
    this.map.animateToRegion(this.region, 100);
  }

  onPressZoomOut() {
    this.region = {
      latitude: this.state.focusedLocation.latitude,
      longitude: this.state.focusedLocation.longitude,
      latitudeDelta: this.state.focusedLocation.latitudeDelta / 2,
      longitudeDelta: this.state.focusedLocation.longitudeDelta / 2
    }
    this.setState({
      focusedLocation: {
        latitudeDelta: this.region.latitudeDelta,
        longitudeDelta: this.region.longitudeDelta,
        latitude: this.region.latitude,
        longitude: this.region.longitude
      }
    })
    this.map.animateToRegion(this.region, 100);
  }

  onMapLayout (){
    const { Titulo } = this.props.route.params;
    const { longitud } = this.props.route.params;
    const { latitud } = this.props.route.params;
    const { imagen } = this.props.route.params;
    const Lot = parseFloat(longitud);
    const Lat = parseFloat(latitud);
    this.setState({Titulo:Titulo,imagen:imagen });
    this.setState({
      focusedLocation: {
        latitude: Lat,
        longitude: Lot,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      }
    })
  }
  async componentDidMount(){
      await this.onMapLayout()
  }

  render() {
    const GOOGLE_MAPS_APIKEY = 'AIzaSyAjINZy57I8YBGKyXDcuSB1AXYeA9UC754';
    const origin = {latitude: -16.4288508, longitude: -71.5038502};
    const destination = {latitude: this.state.focusedLocation.latitude, longitude: this.state.focusedLocation.longitude};
    return (
      <ScrollView>
        <View style={styles.container}>
          <MapView
            region={this.state.focusedLocation}
            provider={PROVIDER_GOOGLE}
            style={styles.map}
            onPress={this.pickLocationHandler}
            showsUserLocation={true}
            followUserLocation={true}
            zoomEnabled={true}
            mapType='standard'
            ref={ref => this.map = ref}
          >
              <Marker
                  coordinate={{
                      latitude: this.state.focusedLocation.latitude,
                      longitude: this.state.focusedLocation.longitude,
                  }}>
                  <Image source = {{uri: this.state.imagen}} style={{height: 50, width:50, borderBottomRightRadius:25, borderBottomLeftRadius:25, borderWidth: 5,borderColor:'#003c8f'}} />
                  <Callout>
                      <View
                          style={{
                              backgroundColor: "#FFFFFF",
                              borderRadius: 5,
                          }}>
                          <Text>
                              {this.state.Titulo}
                          </Text>
                      </View>
                  </Callout>
              </Marker>
              <Marker
                  coordinate={{
                      latitude: -16.4288508,
                      longitude: -71.5038502,
                  }}>
                  <Callout>
                      <View
                          style={{
                              backgroundColor: "#FFFFFF",
                              borderRadius: 5,
                          }}>
                          <Text>
                              TECSUP
                          </Text>
                      </View>
                  </Callout>
              </Marker>
              <MapViewDirections
                origin={origin}
                destination={destination}
                apikey={GOOGLE_MAPS_APIKEY}
                strokeWidth={3}
                strokeColor="#003c8f"
              />
              </MapView>
               
              <View style={styles.button} >
                <TouchableOpacity
                    style={styles.zoomIn}
                    onPress={() => { this.onPressZoomOut() }}
                  >
                    <MaterialCommunityIcons
                      name="zoom-in"
                      style={styles.icon}
                      size={20}
                      color="#fff"
                    />
                </TouchableOpacity>
                <TouchableOpacity
                      style={styles.zoomOut}
                      onPress={() => { this.onPressZoomIn() }}
                    >
                      <MaterialCommunityIcons
                        name="zoom-out"
                        style={styles.icon}
                        size={20}
                        color="#fff"
                      />
                </TouchableOpacity>
            </View>
          </View>
      </ScrollView>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F2C53D',
    },
    map: {
      height: height,
      width: width,
    },
    search:{
      position: "absolute",
    },
    button: {
      top: heightbotones,
      backgroundColor: '#003c8f',
      position: "absolute",
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "space-around",
      padding: 5,
      width:150,
      borderRadius: 10
    },
});
