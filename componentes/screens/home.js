import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, FlatList, SafeAreaView } from 'react-native';
import { Avatar, Badge, ActivityIndicator } from 'react-native-paper';
import firebase from './firebase';
/*pilco XD estubo  AQUI*/
function Home(props) {
  const { navigation } = props
  const [restaurant, setRestaurant] = useState([])
 
  const food = [
    {id:1, title: "Pizza", image: require('../assets/img/icons/pizza.png') },
    {id:2, title: "Menu", image: require('../assets/img/icons/menu.png') },
    {id:3, title: "Parrilla", image: require('../assets/img/icons/carne.png') } ,
    {id:4, title: "Mar", image: require('../assets/img/icons/ceviche.png') } ,
    {id:5, title: "Picanteria", image: require('../assets/img/icons/chile.png') } ,
    {id:6, title: "Vegetariana", image: require('../assets/img/icons/vegetariano.png') } ,
  ]

  useEffect(() => { 
    getResponse();
  }, []);

  const getResponse = async () => {
    let data = []
    await firebase.database().ref('/restaurant').orderByChild('valoracion').limitToLast(4)
    .once('value', function(snapshot) {
      snapshot.forEach(function(childSnapshot) {
        var childData = childSnapshot.val();
        data.push(childData)
      });
    });
    setRestaurant(data)
  };

  return (
    <View style={styles.container}>
      <Text style={styles.text}>Tipos de comidas</Text>
        <SafeAreaView>
            <FlatList style={{padding: 5}}
                data={food}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                legacyImplementation={false}
                numColumns={1}
                keyExtractor= {(item) => {
                  return item.id;
                }}
                renderItem={({item}) => {
                  return (
                    <TouchableOpacity style={styles.iconCard} >
                      <Avatar.Image style={{backgroundColor: 'transparent'}} size={50} source={item.image} />
                      <View>
                        <View >
                          <Text style={{fontSize: 10}}>{item.title}</Text>
                        </View>
                      </View>
                    </TouchableOpacity>
                  )
                }} />
                </SafeAreaView>
      <Text style={styles.text}>Restaurants mejor valorados</Text>
      <FlatList style={styles.list}
          contentContainerStyle={styles.listContainer}
          data={restaurant}
          horizontal={false}
          numColumns={2}
          ListEmptyComponent={() => (<ActivityIndicator style={{marginTop: 25}} animating={true} color='#800000' />)}
          keyExtractor= {(item) => {
            return item.id;
          }}
          renderItem={({item}) => {
            return (
              <TouchableOpacity style={styles.card}  >
                <Badge size={24}>{item.valoracion}</Badge>
                <Avatar.Image size={80} source={{uri: item.imagen}} style={styles.cardImage} />
                <View style={styles.cardHeader}>
                  <View style={{alignItems:"center", justifyContent:"center"}}>
                    <Text style={styles.title}>{item.titulo}</Text>
                  </View>
                </View>
                <View style={styles.cardContent}>
                  <Text>{item.informacion}</Text>
                </View>
              </TouchableOpacity>
            )
          }}/>
    </View>
  )
}

const styles = StyleSheet.create({
  container:{
    flex:1, backgroundColor: '#F2C53D'
  },
  text: {
    color: '#101010',
    fontSize: 24,
    fontWeight: 'bold',
    paddingHorizontal: 20,
    color:"#800000"
  },
  list: {
    paddingHorizontal: 5
  },
  listContainer:{
    alignItems:'center'
  },
  /******** card **************/
  iconCard:{
    backgroundColor:"#ffe4b5",
    alignItems:"center",
    shadowColor: '#00000021',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    marginVertical: 2,
    shadowRadius: 7.49,
    elevation: 12,
    marginHorizontal: 5,
    justifyContent:"center",
    height: 85, width: 80
  },
  card:{
    shadowColor: '#00000021',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    justifyContent: 'space-between',
    elevation: 12,
    marginVertical: 10,
    backgroundColor:"#ffe1ad",
    flexBasis: '42%',
    marginHorizontal: 10,
  },
  cardHeader: {
    paddingVertical: 10,
    paddingHorizontal: 16,
    borderTopLeftRadius: 1,
    borderTopRightRadius: 1,
    flexDirection: 'row',
    alignItems:"center", 
    justifyContent:"center"
  },
  cardContent: {
    paddingHorizontal: 16,
    paddingBottom: 8
  },
  cardFooter:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 8,
    paddingHorizontal: 16,
    borderBottomLeftRadius: 1,
    borderBottomRightRadius: 1,
  },
  cardImage:{
    alignSelf:'center'
  },
  title:{
    fontSize:20,
    fontWeight: 'bold',
    flex:1,
    alignSelf:'center',
    color:"#800000"
  },
})

export default Home