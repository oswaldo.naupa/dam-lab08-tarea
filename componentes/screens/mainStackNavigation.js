import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';

import Login from './login';
import Home from './home';
import Order from './order';
import Perfil from './perfil';
import Registrarse from './registrarse';
import ListaUsuarios from './restaurant/ListaRestaurantes'
import AgregarUsuarios from './restaurant/Mapa';
import EditarUsuarios from './restaurant/Detalles';
import TopNavigation from './TopNavigation'


const Stack = createStackNavigator();
const stackrestaurant = createStackNavigator();
function RestaurantStackScreen(){
  return(
    <stackrestaurant.Navigator>
    <stackrestaurant.Screen name="Restaurantes" component={ListaUsuarios} 
      options={({navigation}) => (
        { 
        title: 'Restaurantes',
        headerRight:()=><TopNavigation navigation={navigation}></TopNavigation>,
        headerStyle: {
          backgroundColor: '#1565c0',
        },
          headerTintColor: '#fff',
          headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 30,
        },
      }
    )}
    />
    <stackrestaurant.Screen name="Informacion" component={AgregarUsuarios} 
      options={({navigation}) => (
        { 
        title: 'Informacion',
        headerRight:()=><TopNavigation navigation={navigation}></TopNavigation>,
        headerStyle: {
          backgroundColor: '#1565c0',
        },
          headerTintColor: '#fff',
          headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 30,
        },
      }
    )}
    />
    <stackrestaurant.Screen name="Mapa" component={EditarUsuarios} 
      options={({navigation}) => (
        { 
        title: 'Ubicacion',
        headerRight:()=><TopNavigation navigation={navigation}></TopNavigation>,
        headerStyle: {
          backgroundColor: '#1565c0',
        },
          headerTintColor: '#fff',
          headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 30,
        },
      }
    )}
    />
  </stackrestaurant.Navigator>
  );
}
function HomeStackScreen(){
  return(
    <stackrestaurant.Navigator>
    <stackrestaurant.Screen name="Home" component={Home} 
      options={({navigation}) => (
        { 
        title: 'Home',
        headerRight:()=><TopNavigation navigation={navigation}></TopNavigation>,
        headerStyle: {
          backgroundColor: '#1565c0',
        },
          headerTintColor: '#fff',
          headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 30,
        },
      }
    )}
    />
  </stackrestaurant.Navigator>
  );
}
function OrdernStackScreen(){
  return(
    <stackrestaurant.Navigator>
    <stackrestaurant.Screen name="Ordenes" component={Order} 
      options={({navigation}) => (
        { 
        title: 'Ordenes',
        headerRight:()=><TopNavigation navigation={navigation}></TopNavigation>,
        headerStyle: {
          backgroundColor: '#1565c0',
        },
          headerTintColor: '#fff',
          headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 30,
        },
      }
    )}
    />
  </stackrestaurant.Navigator>
  );
}
function perfilStackScreen(){
  return(
    <stackrestaurant.Navigator>
    <stackrestaurant.Screen name="Perfil" component={Perfil} 
      options={({navigation}) => (
        { 
        title: 'Perfil',
        headerRight:()=><TopNavigation navigation={navigation}></TopNavigation>,
        headerStyle: {
          backgroundColor: '#1565c0',
        },
          headerTintColor: '#fff',
          headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 30,
        },
      }
    )}
    />
  </stackrestaurant.Navigator>
  );
}

const Tab = createMaterialBottomTabNavigator();
function MainStackNavigator() {
  return (
    <NavigationContainer>
    <Tab.Navigator
      initialRouteName="Home"
      activeColor="white"
      barStyle={{ backgroundColor: '#1565c0' }}  >
      <Tab.Screen name="Home" component={HomeStackScreen}
                  options={{
                    tabBarLabel: 'Explora',
                    tabBarIcon: ({ color }) => (
                      <MaterialCommunityIcons name="food-fork-drink" color={color} size={26} />
                    ),
                  }} />
      <Tab.Screen name="Restauntes" component={RestaurantStackScreen}
                     options={{
                    tabBarLabel: 'Restauntes',
                    tabBarIcon: ({ color }) => (
                      <MaterialCommunityIcons name="food" color={color} size={26} />
                    ),
                  }} />
      <Tab.Screen name="Perfil" component={perfilStackScreen}
                  options={{
                    tabBarLabel: 'Usuarios',
                    tabBarIcon: ({ color }) => (
                      <MaterialCommunityIcons name="account" color={color} size={26} />
                    ),
                  }} />
    </Tab.Navigator>
    </NavigationContainer>
  )
}

export default MainStackNavigator